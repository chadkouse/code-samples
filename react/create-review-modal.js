import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import ReviewModal from '../review-modal/review-modal';
import CreateReviewButton from '../../containers/create-review-button';

import { GUIDE_CSS_SELECTORS } from '../../../shared/constants/onboarding-guide';

import './create-review-modal.scss';

class CreateReviewModal extends Component {
  state = {
    // set id of modal once
    modalId: this.props.modalId,
  };

  componentDidMount() {
    this.props.getReviewDraft(this.props.path);
  }

  createReview = event => {
    event.preventDefault();
    event.stopPropagation();

    const { path, review, isCreateReviewDisabled } = this.props;
    const form = event.currentTarget;
    this.props.createReview(path, form, review, isCreateReviewDisabled);
  };

  handleRate = rating => this.props.setReviewRating(this.props.path, rating);

  setReviewRatings = (value, tag) =>
    this.props.setReviewRatings(this.props.path, { ...this.props.review.ratings, [tag]: value });

  onChange = e => this.props.setReviewDraft(this.props.path, e.target.name, e.target.value);

  closeModal = () => {
    this.props.closeModal();
  };

  render() {
    const { title, path, review, openedModalId, variant, isTagged, setReviewImage, setReviewVideo } = this.props;
    const { modalId } = this.state;

    const isModalOpened = openedModalId === modalId;

    return (
      <>
        {variant === 'button' && (
          <CreateReviewButton
            className={classNames(GUIDE_CSS_SELECTORS.EDIT_REVIEW, 'create-review-button')}
            modalId={modalId}
          />
        )}
        <ReviewModal
          title={title}
          path={path}
          isModalOpened={isModalOpened}
          onChange={this.onChange}
          closeModal={this.closeModal}
          review={review}
          handleSubmit={this.createReview}
          handleRate={this.handleRate}
          isTagged={isTagged}
          modalTitle="YOUR REVIEW:"
          submitButtonTitle="PUBLISH REVIEW"
          setReviewRatings={this.setReviewRatings}
          setReviewImage={setReviewImage}
          setReviewVideo={setReviewVideo}
        />
      </>
    );
  }
}

CreateReviewModal.propTypes = {
  path: PropTypes.string,
  variant: PropTypes.oneOf(['button', 'link']),
  title: PropTypes.string.isRequired,

  getReviewDraft: PropTypes.func.isRequired,
  setReviewDraft: PropTypes.func.isRequired,
  setReviewRating: PropTypes.func.isRequired,
  setReviewRatings: PropTypes.func.isRequired,
  setReviewImage: PropTypes.func.isRequired,
  setReviewVideo: PropTypes.func.isRequired,
  createReview: PropTypes.func.isRequired,

  review: PropTypes.object.isRequired,

  isCreateReviewDisabled: PropTypes.bool.isRequired,
  modalId: PropTypes.string,
  openedModalId: PropTypes.string,

  closeModal: PropTypes.func.isRequired,
  isTagged: PropTypes.bool,
};

CreateReviewModal.defaultProps = {
  path: '',
  modalId: '',
  openedModalId: '',
  variant: 'button',
  isTagged: false,
};

export default CreateReviewModal;
