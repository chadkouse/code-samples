//
//  EarcandyTenorApiManager.swift
//  EarcandyCore
//
//  Created by Chad Kouse on 8/19/18.
//

import Foundation

class EarcandyTenorApiManager : NSObject {
    
    var apiKey : String? = nil
    
    struct MediaProps: Codable {
        let url : String
        let dims : [Int]
        let duration: Float?
        let preview: String
        let size: Int?
    }
    
    struct Media : Codable {
        let nanomp4 : MediaProps?
        let nanowebm : MediaProps?
        let nanogif : MediaProps?
        let tinygif : MediaProps?
        let tinymp4 : MediaProps?
        let tinywebm : MediaProps?
        let mediumgif : MediaProps?
        let webm : MediaProps?
        let gif : MediaProps?
        let mp4 : MediaProps?
        let loopedmp4 : MediaProps?
    }
    
    struct GifResult: Codable {
        let id: String
        let itemurl: String
        let media: [Media]
        let url: String
        let hasaudio : Bool
        let title : String
    }
    
    struct GifResults: Codable {
        let results: [GifResult]
    }
 
    
    //Singleton
    private static var privateShared: EarcandyTenorApiManager?
    
    var gifRequest : URLSessionDataTask?
    var latestGifRequest : URLSessionDataTask?
    
    class func shared() -> EarcandyTenorApiManager {
        guard let manager = privateShared  else {
            privateShared = EarcandyTenorApiManager()
            return privateShared!
        }
        return manager
    }
    
    private override init() {
        if  let infoPlist = Bundle.main.infoDictionary,
            let key = infoPlist["ECTenorApiKey"] as? NSString {
            self.apiKey = key as String;
        }
    }
    deinit {
        print("deinit \(#file)")
    }

    private func getUserId(callback: @escaping (String)->()) {
        guard let apikey = apiKey else {
            print("Tried to make tenor api request with no api key defined in the main target's Info.plist")
            callback("")
            return
        }
        
        /*
         If this is a returning user, grab their stored anonymous ID and jump directly to getting data
         Otherwise this is a new user, so start the flow by getting an anonymous id and having the callback store it & pass it to requestData
         */
        if let anonymousID = UserDefaults.standard.string(forKey: "anonymousID") {
            callback(anonymousID)
            return
        } else {
            let anonymousIDRequest = URLRequest(url: URL(string: String(format: "https://api.tenor.com/v1/anonid?key=%@", apikey))!)
            makeWebRequest(urlRequest: anonymousIDRequest) { (response) in
                // Read the anonymous id for the user
                let anonymousID = response["anon_id"] as! String
                
                // Store the anonymousID for use in later API calls
                UserDefaults.standard.setValue(anonymousID, forKey: "anonymousID")
                
                callback(anonymousID)
                return
            }
        }
        
    }

    /**
     Execute web request to retrieve the top GIFs returned(in batches of 8) for the given search term.
     */
    func requestData(searchTerm: String, limit: Int = 8, callback: @escaping (GifResults)->())
    {
        guard let apikey = apiKey else {
            print("Tried to make tenor api request with no api key defined in the main target's Info.plist")
            return;
        }
        
        self.getUserId { (anonymousID) in
            // make initial search request for the first 8 items
            let searchRequest = URLRequest(url: URL(string: String(format: "https://api.tenor.com/v1/search?q=%@&key=%@&anon_id=%@&limit=%d",
                                                                   searchTerm.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!,
                                                                   apikey,
                                                                   anonymousID,
                                                                   limit))!)
            
            self.makeGifWebRequest(urlRequest: searchRequest, callback: callback)
        }
    }

    /**
     Async URL requesting function.
     */
    private func makeWebRequest(urlRequest: URLRequest, callback: @escaping ([String:AnyObject]) -> ())
    {
        // Make the async request and pass the resulting json object to the callback
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject] {
                    // Push the results to our callback
                    callback(jsonResult)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    /**
     Async URL requesting function.
     */
    private func makeGifWebRequest(urlRequest: URLRequest, callback: @escaping (GifResults) -> ())
    {
        gifRequest?.cancel()
        print("calling \(urlRequest)")
        
        // Make the async request and pass the resulting json object to the callback
        gifRequest = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            do {
                if let d = data {
                  let gifs = try JSONDecoder().decode(GifResults.self, from: d)
                  callback(gifs)
                }
            } catch {
                print("Error getting gifs")
            }
        }
        gifRequest?.resume()
    }

}
