package com.example.opinionspage.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.opinionspage.entities.Url

@Dao
interface UrlDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUrl(url: Url): Long?

    @Query("SELECT * FROM url ORDER BY ts DESC LIMIT 5")
    fun getLastFiveUrls(): List<Url>

    @Query("SELECT * FROM url ORDER BY ts DESC LIMIT 1")
    fun getLastUrl(): List<Url>
}
