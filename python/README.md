### Python samples

`get_lb.py` - a quick script that uses aws boto3 library to take a url and
return ssh connection information for all instances that respond to that url.
