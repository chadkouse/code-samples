## Kotlin samples

`MainActivity.kt` - Deals with getting the services this app needs installed
along with the initial data loads.

`UrlDao.kt` - Dao dealing with getting urls from the room db
