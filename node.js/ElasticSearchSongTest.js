var Sayic = require('../');
var ElasticSearch = require('../lib/search/es');
var helper = require('./test-helper');
var sinon = require('sinon');

var should = require('should');
var sandbox;
var internals = {
};

describe('ElasticSearchSong', function () {

    beforeEach(function () {
        sandbox = sinon.sandbox.create();
    });

    afterEach(function () {
        sandbox.restore();
    });

    describe('#putSong()', function () {
        it('should add a song to search index', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "index")
                .yields(null);
            Sayic.ElasticSearchSong.putSong(helper.mockSong(), done);
        });
        
        it ('should add a song with suggested names', function(done) {
            sandbox.stub(ElasticSearch.getClient(), "index")
                .yields(null);
            var song = helper.mockSong();
            song.set({name_suggest: "foo"});
            song.set({artist_suggest: "foo"});
            Sayic.ElasticSearchSong.putSong(song, done);
        })
    });

    describe('#getAllSongs()', function () {
        it('should work with a blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            Sayic.ElasticSearchSong.getAllSongs(null, null, null, undefined, undefined, done);
        });
        
        it('should work with a blank genre search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            Sayic.ElasticSearchSong.getAllSongs(null, null, "bar", 0, 1, done);
        });
        
        it('should work with a blank sentiment search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            Sayic.ElasticSearchSong.getAllSongs("foo", null, null, 0, 1, done);
        });
        
        it('should work with a non blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            Sayic.ElasticSearchSong.getAllSongs("foo", null, "bar", 0, 1, done);
        });

    });

    describe('#getAllArtists()', function () {
        it('should work with a blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getAllArtists(null, null, null, undefined, undefined, done);
        });

        it('should work with a blank genre search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getAllArtists(null, null, "bar", 0, 1, done);
        });

        it('should work with a blank sentiment search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getAllArtists("foo", null, null, 0, 1, done);
        });

        it('should work with a non blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getAllArtists("foo", null, "bar", 0, 1, done);
        });

    });

    describe('#getSongBySearch()', function () {
        it('should generate an error with a blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongBySearch(null, null, null, null, undefined, undefined, function(err) {
                err.should.be.ok;
                done();
            });
        });

        it('should work with a non blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongBySearch("foo", "bar", null, 1, 0, 1, done);
        });

    });

    describe('#getTopSongsForSentiment', function () {
        it('should generate an error with a null sentiment', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getTopSongsForSentiment(null, null, null, null, function(err) {
                err.should.be.ok;
                done();
            });
        });

        it('should not generate an error with a undefined offset or limit', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getTopSongsForSentiment("foo", null, undefined, undefined, function(err) {
                should(err).not.exist;
                done();
            });
        });

        it('should not generate an error with a defined offset or limit', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);

            Sayic.ElasticSearchSong.getTopSongsForSentiment("foo", null, 0, 10, function(err) {
                should(err).not.exist;
                done();
            });
        });

        it('should handle client error', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(new Error("Test Error"));

            Sayic.ElasticSearchSong.getTopSongsForSentiment("foo", null, 0, 10, function(err) {
                err.should.be.ok;
                done();
            });
        });

        it('should return results without error', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null, {
                    facets: {
                        songId: {
                            terms: [{term: "foo"}]
                        }
                    }
                });

            Sayic.ElasticSearchSong.getTopSongsForSentiment("foo", null, 0, 10, done);
        });

    })

    describe('#getSongByArtistTitle()', function () {
        it('should generate an error with a blank artist', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongByArtistTitle(null, null, undefined, undefined, function(err) {
                err.should.be.ok;
                done();
            });
        });
        
        it('should generate an error with a blank title', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongByArtistTitle("foo", null, undefined, undefined, function(err) {
                err.should.be.ok;
                done();
            });
        });

        it('should work with missing offset and limit', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongByArtistTitle("foo", "bar", undefined, undefined, done);
        });

        it('should work with a non blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongByArtistTitle("foo", "bar", 0, 1, done);
        });

    });

    describe('#getSongBySentimentSearch())', function () {
        it('should generate an error with a blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongBySentimentSearch(null, undefined, undefined, function (err) {
                err.should.be.ok;
                done();
            });
        });

        it('should work with missing offset and limit', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongBySentimentSearch("foo", undefined, undefined, done);
        });

        it('should work with a non blank search', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "search")
                .yields(null);
            
            Sayic.ElasticSearchSong.getSongBySentimentSearch("foo", 0, 1, done);
        });
    });
    
    describe('#autoCompletArtist())', function () {
        it('should work without error', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "suggest")
                .yields(null);
            
            Sayic.ElasticSearchSong.autoCompleteArtist("foo", 0, 5, done);
        });
    });

    describe('#autoCompletSong())', function () {
        it('should work without error', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "suggest")
                .yields(null);
            
            Sayic.ElasticSearchSong.autoCompleteSong("foo", 0, 5, done);
        });
    });

    describe('#deleteSong()', function () {
        it('should delete a song from search index', function (done) {
            sandbox.stub(ElasticSearch.getClient(), "delete")
                .yields(null);
            
            Sayic.ElasticSearchSong.deleteSong(helper.mockSong().get("id"), done);
        });
    });

})
