package com.example.opinionspage

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.opinionspage.entities.Setting
import com.example.opinionspage.entities.Url
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




const val API_DOMAIN = "p.webwrap.com"
const val API_URL = "https://$API_DOMAIN/"

class MainActivity : AppCompatActivity() {
    private var showingAccessibilityPrompt = false
    private var showingPermissionPrompt = false
    private val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084
    private var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        appDatabase = AppDatabase.getDatabase(this)!!
        if (accessibilityPrompt()) {
            startOverlayIfNeeded(true)
        }

        switch1.setOnCheckedChangeListener { buttonView, isChecked ->
            val pref = PreferenceManager.getDefaultSharedPreferences(this)
            pref.edit().putBoolean("showbutton", isChecked).commit()
            if (!isChecked) {
                Intent(this, OverlayService::class.java).also { intent ->
                    intent.putExtra("visibility", View.GONE)
                    startService(intent)
                }
            }
        }
        setup()
    }

    companion object {

        private val INTENT_URL = "url"

        fun newIntent(context: Context, url: String): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(INTENT_URL, url)
            return intent
        }
    }

    override fun onResume() {
        super.onResume()
        appDatabase = AppDatabase.getDatabase(this)!!
        if (accessibilityPrompt()) {
            startOverlayIfNeeded(true)
        }
        setup()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            startOverlayIfNeeded(false)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
        setup()
    }

    private fun setup() {
        webviewcontainer.visibility = View.GONE
        GetSettingFromDb(this).execute()
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        var showButton = pref.getBoolean("showbutton", true)
        switch1.isChecked = showButton
        val url = pref.getString("url", "")
        var theUrl = Url()
        theUrl.ts = 0
        theUrl.url = url
        val adapter = UrlListAdapter(this, R.layout.url_item, listOf(theUrl))
        this.tvDatafromDb.adapter = adapter

        webview.settings.javaScriptEnabled = true
        webview.settings.userAgentString = webview.settings.userAgentString.replace("; wv", "")
        webview.settings.allowContentAccess = true
        webview.settings.databaseEnabled = true
        webview.settings.domStorageEnabled = true
        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return false
            }
        }
        val message = intent.getStringExtra("check_url") ?: ""
        intent.removeExtra("check_url")
        if (message != "") {
            GetSettingFromDb2(this).execute(message)
            return
        } else {

            val url = intent.getStringExtra(INTENT_URL)
                ?: ""
            if (url != "") {
                webview.loadUrl(url)
                webviewcontainer.visibility = View.VISIBLE
            } else {
                webviewcontainer.visibility = View.GONE
            }

            close_web.setOnClickListener {
                webviewcontainer.visibility = View.GONE
            }
        }

        btn_send_email.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "info@webwrap.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "WebWrap for Android Beta - Support")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Your message: ")
            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }

    }

    private fun setupSpinner(selected: String?) {
        val context = this
        ArrayAdapter.createFromResource(
            this,
            R.array.display_options_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter

            //preselet the saved value if needed
            selected?.let {
                val spinnerPosition = adapter.getPosition(it)
                spinner.setSelection(spinnerPosition)
            }

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val setting = Setting()
                    setting.id = "opener"
                    setting.value = parent?.getItemAtPosition(position) as String?
                    SetSettingToDb(context.appDatabase!!).execute(setting)
                }
            }
        }
    }

    private fun startOverlayIfNeeded(showSettingsIfNeeded: Boolean) {
        //Check if the application has draw over other apps permission or not?
        //This permission is by default available for API<23. But for API > 23
        //you have to ask for the permission in runtime.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            if (showSettingsIfNeeded && !showingPermissionPrompt) {
                showingPermissionPrompt = true
                val alertDialogBuilder = AlertDialog.Builder(this)
                alertDialogBuilder.setTitle(getString(R.string.allow_permission))
                alertDialogBuilder.setMessage(R.string.draw_over_permission_disabled)
                alertDialogBuilder.setPositiveButton(R.string.yes) { _, _ ->
                    showingPermissionPrompt = false
                    //If the draw over permission is not available open the settings screen
                    //to grant the permission.
                    val intent = Intent(
                        Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:$packageName")
                    )
                    startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
                }

                alertDialogBuilder.setNegativeButton(R.string.not_now) { _, _ ->
                    showingPermissionPrompt = false
                }
                alertDialogBuilder.show()
            }
        } else {
            showingPermissionPrompt = false
            initializeView()
        }
    }

    private fun initializeView() {
        println("************** STARTING OVERLAY *****************")
        startService(Intent(this@MainActivity, OverlayService::class.java))
    }

    private fun accessibilityPrompt(): Boolean {
        if (showingAccessibilityPrompt) return false
        showingAccessibilityPrompt = true

        if (!isAccessibilityEnabled()) {
            message.text = getString(R.string.accessibility_service_disabled_message)
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle(R.string.accessibility_service_disabled_alert_title)
            alertDialogBuilder.setMessage(R.string.accessibility_service_disabled_alert_message)
            alertDialogBuilder.setPositiveButton(R.string.yes) { _, _ ->
                showingAccessibilityPrompt = false
                openAccessibilitySettings()
            }

            alertDialogBuilder.setNegativeButton(R.string.not_now) { _, _ ->
                showingAccessibilityPrompt = false
            }
            alertDialogBuilder.show()
            return false
        } else {
            message.text = getString(R.string.accessibility_service_enabled_message)

            return true
        }
    }

    private fun openAccessibilitySettings() {
        val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
        startActivityForResult(intent, 0)
    }

    private fun isAccessibilityEnabled(): Boolean {
        var accessibilityEnabled = 0
        val accessibilityFound = false
        try {
            accessibilityEnabled =
                Settings.Secure.getInt(this.contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
            println("ACCESSIBILITY: $accessibilityEnabled")
        } catch (e: SettingNotFoundException) {
            println("Error finding setting, default accessibility to not found: " + e.message)
        }

        val mStringColonSplitter = TextUtils.SimpleStringSplitter(':')

        if (accessibilityEnabled == 1) {
            println("***ACCESSIBILITY IS ENABLED***: ")


            val settingValue =
                Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
            if (settingValue != null) {
                println("Setting: $settingValue")
                mStringColonSplitter.setString(settingValue)
                while (mStringColonSplitter.hasNext()) {
                    val accessibilityService = mStringColonSplitter.next()
                    println("Setting: $accessibilityService")
                    if (accessibilityService.equals(
                            "com.example.opinionspage/com.example.opinionspage.MyService",
                            ignoreCase = true
                        )
                    ) {
                        println("We've found the correct setting - accessibility is switched on!")
                        return true
                    }
                }
            }

            println("***END***")
        } else {
            println("***ACCESSIBILITY IS DISABLED***")
        }
        return accessibilityFound
    }

    private class GetSettingFromDb(var context: MainActivity) : AsyncTask<Void, Void, Setting>() {
        override fun doInBackground(vararg params: Void?): Setting {
            return context.appDatabase!!.settingDao().getSetting("opener")
        }

        override fun onPostExecute(setting: Setting?) {
            context.setupSpinner(setting?.value)
        }
    }

    private class SetSettingToDb internal constructor(private val mAsyncDb: AppDatabase) :
        AsyncTask<Setting, Void, Void>() {
        override fun doInBackground(vararg params: Setting): Void? {
            mAsyncDb.settingDao().insertSetting(params[0])
            return null
        }
    }

    private class GetDataFromDb(var context: MainActivity) : AsyncTask<Void, Void, List<Url>>() {
        override fun doInBackground(vararg params: Void?): List<Url> {
            return context.appDatabase!!.urlDao().getLastUrl()
        }

        override fun onPostExecute(urlList: List<Url>?) {
            if (urlList!!.isNotEmpty()) {
                val adapter = UrlListAdapter(context, R.layout.url_item, urlList)
                context.tvDatafromDb.adapter = adapter
            }
        }
    }

    private class GetSettingFromDb2(var context: MainActivity) :
        AsyncTask<String, Void, OverlayService.OpenerSettings?>() {
        override fun doInBackground(vararg params: String?): OverlayService.OpenerSettings? {
            params?.let {
                it[0]?.let { url ->
                    val openerSetting = OverlayService.OpenerSettings()
                    openerSetting.opener = context.appDatabase!!.settingDao().getSetting("opener")
                    openerSetting.url = url
                    return openerSetting
                }
            }
            return null
        }

        override fun onPostExecute(setting: OverlayService.OpenerSettings?) {
            setting?.let {
                if (setting.opener.value == "New Browser tab") {
                    val openUrl = Intent(Intent.ACTION_VIEW)
                    openUrl.data = Uri.parse(API_URL + "#" + setting.url)
                    openUrl.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    context.startActivity(openUrl)
                }
            }
        }
    }

}
