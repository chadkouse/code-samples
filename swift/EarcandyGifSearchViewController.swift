//
//  EarcandyGifSearchViewController.swift
//  EarcandyCore
//
//  Created by Chad Kouse on 3/24/19.
//

import Foundation

protocol EarcandyGifSearchViewControllerDelegate: class {
    func selectedResult(_ sender : EarcandyGifSearchViewController, result: EarcandyTenorApiManager.GifResult)
    func canceled(_ sender : EarcandyGifSearchViewController)
}

class EarcandyGifSearchViewController : UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak private var searchBar: UISearchBar!
    @IBOutlet weak private var collectionView: UICollectionView!
    
    // MARK: Variables
    var delegate : EarcandyGifSearchViewControllerDelegate?
    let gifManager = EarcandyTenorApiManager.shared()
    var gifResults = EarcandyTenorApiManager.GifResults(results: [])
    var searchText : String?
    
    override func viewWillAppear(_ animated: Bool) {
        if let s = searchText {
            if s.length > 0 {
                searchBar.text = s
                runSearch()
                return
            }
        }
        searchBar.becomeFirstResponder()
        
    }
    
    @objc func runSearch() {
        guard let searchText = searchBar.text else { return }
        
        
        gifManager.requestData(searchTerm: searchText, limit: 24) { (response) in
            self.gifResults = response
            OnMainThread {
                self.collectionView.reloadData()
            }
            
        }
        
    }
    
}

extension EarcandyGifSearchViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let d = self.delegate else {
            return
        }
        if gifResults.results.indices.contains(indexPath.row) {
            let result = gifResults.results[indexPath.row]
            d.selectedResult(self, result: result)
        }
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
        }    }
    
}

extension EarcandyGifSearchViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gifResults.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let gifCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gifcell", for: indexPath)
        let imgView = gifCell.viewWithTag(1) as! UIImageView
        if gifResults.results.indices.contains(indexPath.row) {
            let result = gifResults.results[indexPath.row]
            if result.media.count > 0 {
                let media = result.media.first!
                if let gifURL = URL(string: (media.gif?.url)!) {
                    imgView.kf.cancelDownloadTask()
                    imgView.kf.indicatorType = .activity
                    imgView.kf.setImage(with: gifURL)
                }
            }
        }
        return gifCell
    }
    
    
}

extension EarcandyGifSearchViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.runSearch), object: nil)
        self.perform(#selector(self.runSearch), with: nil, afterDelay: 0.5)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
