var Sayic = require('./../core.js');
var ElasticSearch = require('./es.js');
var CONFIG = require('config');
var Hoek = require('hoek');

Sayic.ElasticSearchSong = {
    indexName: CONFIG.ElasticSearch.index,
    typeName: 'song',
    getSongByArtistTitle: function (options, callback) {
        var defaults = {
            offset: 0,
            limit: 1
        };
        var config = Hoek.applyToDefaults(defaults, options);
        
        if (!config.artist || typeof config.artist == 'undefined' || config.artist == '') {
            callback(new Error("artist was blank"), null);
            return;
        }
        if (!config.name || typeof config.name == 'undefined' || config.name == '') {
            callback(new Error("artist was blank"), null);
            return;
        }

        var qryObj = {
            "query": {
                "bool": {
                    "must": [
                        {"term": {"artist.lower": config.artist.toLowerCase()}},
                        {"term": {"name.lower": config.name.toLowerCase()}}
                    ]
                }
            },
            "from": config.offset,
            "size": config.limit
        };

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj},
            function (err, result) {
                if (err) return callback(err);
                if (result && result.hits && result.hits.hits && result.hits.hits.length > 0) {
                    var s = new Sayic.Song(result.hits.hits[0]._source);
                    return callback(null, {song: s});
                }
                return callback();
            });
    },
    getSongByiTunesId: function (options, callback) {
        var defaults = {
            offset: 0,
            limit: 1,
            iTunesId: null
        }
        
        var config = Hoek.applyToDefaults(defaults, options);
        
        if (!config.iTunesId || typeof config.iTunesId == 'undefined' || config.iTunesId == '') {
            callback(new Error("search was blank"), null);
            return;
        }

        var qryObj = {
            "query": {
                "term": { "itunes_id": config.iTunesId }
            }
        };

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                if (result && result.hits && result.hits.hits && result.hits.hits.length > 0) {
                    var s = new Sayic.Song(result.hits.hits[0]._source);
                    return callback(null, {song: s});
                }
                return callback();
            });
    },
    getSongBySentimentSearch: function (search, offset, limit, callback) {
        if (!search || typeof search == 'undefined' || search == '') {
            callback(new Error("search was blank"), null);
            return;
        }

        offset = typeof offset != 'undefined' ? offset : 0;
        limit = typeof limit != 'undefined' ? limit : 10;

        var qryObj = {
            "query": {
                "term": { "sentiment": search }
            }
        };

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                callback(null, result);
            });
    },
    putSong: function (song, callback) {
        var client = ElasticSearch.getClient();
        var body = song.toJSON();
        if (body.name_suggest) {
            body.name_suggest = {
                "input": body.name_suggest,
                "output": body.name + " by " + body.artist,
                "payload": {"artist": body.artist, "name": body.name}
            }
        }
        if (body.artist_suggest) {
            body.artist_suggest = {
                "input": body.artist_suggest,
                "output": body.artist,
                "payload": { "artist": body.artist}
            }
        }
        client.index({
                index: this.indexName,
                type: this.typeName,
                id: song.get("id"),
                body: body
            },
            function (err, data) {
                if (err) return callback(err);
                callback(null, data);
            });

    },
    deleteSong: function (songId, callback) {
        var client = ElasticSearch.getClient();
        client.delete({
                index: this.indexName,
                type: this.typeName,
                id: songId
            },
            function (err, data) {
                if (err) return callback(err);
                callback(null, songId);
            });
    },
    autoCompleteArtist: function (prefix, offset, limit, callback) {
        this.autoComplete("artist_suggest", prefix, offset, limit, callback);
    },
    autoCompleteSong: function (prefix, offset, limit, callback) {
        this.autoComplete("name_suggest", prefix, offset, limit, callback);
    },
    autoComplete: function (field, prefix, offset, limit, callback) {
        offset = typeof offset != 'undefined' ? offset : 0;
        limit = typeof limit != 'undefined' ? limit : 10;
        var internalLimit = offset + limit;
        
        var qryObj = {
            "song": {
                "text": prefix,
                "completion": {
                    "field": field,
                    "size": internalLimit
                }
            }
        };

        var client = ElasticSearch.getClient();
        client.suggest({
                index: this.indexName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                if (result && result.song && result.song[0] && result.song[0].options) {
                    var resultLength = result.song[0].options.length;
                    var slice = 0;
                    if (result.song[0].options.length >= internalLimit)
                        slice = limit;
                    else if (result.song[0].options.length > offset)
                        slice = resultLength - offset;
                    else
                        slice = -1;
                    result.song[0].options = slice >= 0 ? result.song[0].options.slice((-1*slice)) : [];
                }
                callback(null, result);
            });
    },
    getTopArtistsForSentiment: function (sentiment, currentUserId, offset, limit, callback) {
        if (!sentiment || typeof sentiment == 'undefined' || sentiment == '') {
            callback(new Error("search was blank"), null);
            return;
        }

        offset = typeof offset != 'undefined' ? offset : 0;
        limit = typeof limit != 'undefined' ? limit : 10;
        var internalLimit = offset + limit;

        var qryObj =
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "has_child": {
                                "type": "sayic",
                                "query": {
                                    "bool": {
                                        "must": [
                                            {
                                                "match": {
                                                    "_type": "sayic"
                                                }
                                            },
                                            {
                                                "match": {
                                                    "message.lower": sentiment.toLowerCase()
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        },
                        {
                            "term": {
                                "published": true
                            }
                        }
                    ]
                }
            },
            "facets": {
                "artist": {
                    "terms": {
                        "field": "artist.lower",
                        "size": internalLimit
                    }
                }
            },
            "size": 0
        };

        if (currentUserId != Sayic.adminUserId) {
            qryObj.query.bool.must.push({
                "term":{"published":true}
            })
            qryObj.query.bool.must[0].has_child.query.bool.must.push({
                "term": { "isPublic": true}
            });
        }

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                if (!result || !result || !result.facets
                    || !result.facets["artist"]
                    || !result.facets["artist"].terms
                    || !Array.isArray(result.facets["artist"].terms) ||
                    result.facets["artist"].terms.length <= 0)
                    return callback(null, []);
                var artists = result.facets["artist"].terms.map(function (term) {
                    return term.term;
                });
                callback(null, artists);
            });
    },
    getTopSongsForSentiment: function (sentiment, currentUserId, offset, limit, callback) {
        if (!sentiment) {
            callback(new Error("search was blank"), null);
            return;
        }

        offset = typeof offset != 'undefined' ? offset : 0;
        limit = typeof limit != 'undefined' ? limit : 10;
        var internalLimit = offset + limit;

        var qryObj =
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "has_child": {
                                "type": "sayic",
                                "query": {
                                    "bool": {
                                        "must": [
                                            {
                                                "match": {
                                                    "_type": "sayic"
                                                }
                                            },
                                            {
                                                "match": {
                                                    "message.lower": sentiment.toLowerCase()
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    ]
                }
            },
            "facets": {
                "songId": {
                    "terms": {
                        "field": "id",
                        "size": internalLimit
                    }
                }
            },
            "size": 0
        };
        
        if (currentUserId != Sayic.adminUserId) {
            qryObj.query.bool.must.push({
                "term":{"published":true}
            })
            qryObj.query.bool.must[0].has_child.query.bool.must.push({
                "term": { "isPublic": true}
            });
        }

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                if (!result || !result || !result.facets
                    || !result.facets["songId"]
                    || !result.facets["songId"].terms
                    || !Array.isArray(result.facets["songId"].terms) ||
                    result.facets["songId"].terms.length <= 0)
                    return callback(null, []);
                var songIds = result.facets["songId"].terms.map(function (term) {
                    return term.term;
                });
                callback(null, songIds);
            });
    },
    getSongMissingISRC: function (callback) {
        var qryObj = {
            "query": {
                "function_score": {
                    "filter": {
                        "missing": {
                            "field": "isrc"
                        }
                    },
                    "functions": [
                        {
                            "random_score": {
                            }
                        }
                    ],
                    "score_mode": "sum"
                }
            },
            "size": 1
        };
        
        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                var total = 0;
                if (!result || !result.hits || !result.hits.hits || result.hits.hits.length <= 0)
                    return callback(null, []);
                if (result && result.hits && result.hits.total)
                    total = result.hits.total;
                var song = new Sayic.Song(result.hits.hits[0]._source);
                callback(null, {total: total, song: song});
            });
        
    },
    getSongByISRC: function(options, callback) {
        var defaults = {
            offset: 0,
            limit: 1,
            isrc: null
        }

        var config = Hoek.applyToDefaults(defaults, options);

        if (!config.isrc || typeof config.isrc == 'undefined' || config.isrc == '') {
            callback(new Error("search was blank"), null);
            return;
        }

        var qryObj = {
            "query": {
                "term": { "isrc": config.isrc.toLowerCase() }
            }
        };

        var client = ElasticSearch.getClient();
        client.search({
                index: this.indexName,
                type: this.typeName,
                body: qryObj
            },
            function (err, result) {
                if (err) return callback(err);
                if (result && result.hits && result.hits.hits && result.hits.hits.length > 0) {
                    var s = new Sayic.Song(result.hits.hits[0]._source);
                    return callback(null, {song: s});
                }
                return callback();
            });
        
    }
};

module.exports = Sayic.ElasticSearchSong;

