import { graphqlOperation } from 'aws-amplify';

import API from '../../shared/tools/api-proxy';
import get from '../../shared/tools/get';

import User from '../../shared/tools/user';
import NotificationRecord from '../../shared/tools/notification-record';

import * as types from '../constants/types';

import * as mutations from '../../shared/graphql/mutations';
import * as queries from '../../shared/graphql/queries';
import * as storage from '../tools/storage';

import { buildReview, buildTemporaryReview } from '../tools/build-review';

const checkIfLoginRequired = (session, userId) => {
  if (!session || !userId) {
    User.federatedSignIn();
    return true;
  }

  return false;
};

export const getReviewDraft = path => dispatch => {
  let draft = storage.getReviewDraft(path);

  if (draft) {
    draft = JSON.parse(draft);
    dispatch({
      type: types.SET_REVIEW_CONTENT,
      payload: {
        review: {
          title: draft.title,
          content: draft.content,
          rating: draft.rating,
          ...(draft.picture ? { picture: draft.picture } : {}),
          ...(draft.ratings ? { ratings: draft.ratings } : {}),
        },
      },
    });
  }
};

export const setReviewDraft = (path, field, content) => (dispatch, getState) => {
  const existingDraft = getState().reviews.review;

  dispatch({ type: types.SET_REVIEW_CONTENT_FIELD, payload: { field, content } });

  const draft = JSON.stringify(Object.assign(existingDraft, { [field]: content }));

  storage.setReviewDraft(path, draft);
};

export const setReviewPicture = (path, picture) => dispatch => dispatch(setReviewDraft(path, 'picture', picture));

export const setReviewVideo = (path, picture, content) => dispatch =>
  dispatch({ type: types.SET_REVIEW_CONTENT_FIELD, payload: { field: 'video', content } });

export const setReviewRating = (path, rating) => dispatch => dispatch(setReviewDraft(path, 'rating', rating));

// rating here is an object
export const setReviewRatings = (path, rating) => dispatch => dispatch(setReviewDraft(path, 'ratings', rating));

export const showModal = modalId => (dispatch, getState) => {
  const state = getState();
  const { path } = state.page;

  dispatch({ type: types.RESET_REVIEW_CONTENT });

  dispatch(getReviewDraft(path));

  dispatch({ type: types.SHOW_MODAL, payload: { modalId } });
};

export const closeModal = () => dispatch => {
  dispatch({ type: types.CLOSE_MODAL });
  dispatch({ type: types.RESET_REVIEW_CONTENT });
};

export const createReview = (path, form, review, disabled) => async (dispatch, getState) => {
  const { userId, session } = getState().user;
  const { isTagged } = getState().reviews;

  const { temporaryReview, removeTemporaryReviewCallback } = buildTemporaryReview(review);

  dispatch({ type: types.CREATE_TEMPORARY_REVIEW, payload: temporaryReview });
  dispatch({ type: types.CLOSE_MODAL });

  if (disabled || checkIfLoginRequired(session, userId)) {
    return;
  }

  dispatch({ type: types.DISABLE_REVIEW_CREATION });

  form.checkValidity();

  if (!review.title || !review.content) {
    return;
  }

  let newReview = null;
  try {
    newReview = await buildReview({ review, isTagged, userId, path, added: Math.floor(new Date().getTime() / 1000) });
  } catch (err) {
    console.error(err);
    dispatch({
      type: types.SET_REVIEW_CONTENT_BY_ID,
      payload: {
        id: review.id,
      },
    });

    return;
  }

  try {
    const [createdReviewData, listDiscussionFollowers] = await Promise.all([
      API.graphql(graphqlOperation(mutations.createReview, newReview)),
      API.graphql({
        query: queries.listDiscussionFollowers,
        variables: { path },
        fetchPolicy: 'cache-and-network',
      }),
    ]);

    const createdReview = get(createdReviewData, 'data.createReview', {});
    const userFollowers = get(createdReview, 'user.followers.items', []).map(user => user.follower.id);

    const discussionFollowers = get(listDiscussionFollowers, 'data.listDiscussionFollowers.items', [])
      .map(discussionFollowing => discussionFollowing.user.id)
      .filter(id => id !== userId)
      .filter(id => !userFollowers.includes(id));

    const record = {
      session,
      actor: userId,
      object: 'review',
      objectId: createdReview.id,
      path,
      title: createdReview.title,
    };

    userFollowers.map(user =>
      NotificationRecord.record({
        ...record,
        verb: discussionFollowers.includes(user) ? 'created by followed user on followed page' : 'created',
        user,
      }),
    );

    discussionFollowers
      .filter(id => !userFollowers.includes(id))
      .forEach(user =>
        NotificationRecord.record({
          ...record,
          verb: 'created on followed page',
          user,
        }),
      );

    storage.clearContentDraft(path);
  } catch (err) {
    console.error('Error creating review: ', err);
  }

  dispatch({ type: types.ENABLE_REVIEW_CREATION });
  dispatch({ type: types.RESET_REVIEW_CONTENT });

  if (removeTemporaryReviewCallback) {
    removeTemporaryReviewCallback();
  }
  dispatch({ type: types.REMOVE_TEMPORARY_REVIEW });
};
